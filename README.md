# 3CX Customized IVR Handling

## Idea

* Pre-handling for incoming calls (blocking number, verification of anonymous numbers)
* Saving of user input on call verification
* Push-Message to mobile phones with real number of anonymous call
* Phone-Book lookup for number to categorize numbers in known and unknown numbers
* Call-Back script

## Needed packets

```text
xmlstarlet
curl
```

## Config

### config_piag.xml

```xml
<config>
    <piag>
        <host>dev.piag-systems.de</host>
        <username>xxx</username>
        <password>xxx</password>
    </piag>
</config>
```

Don't forget chown!


### config_fritzbox.xml

```xml
<config>
    <fritzBox>
        <host>fritz.box</host>
        <password>xxx</password>
        <phoneBookBlocklistId>1</phoneBookBlocklistId>
        <phoneBookBlocklistExportName>Rufsperren</phoneBookBlocklistExportName>
        <phoneBookId>240</phoneBookId>
        <phoneBookExportName>Telefonbuch</phoneBookExportName>
    </fritzBox>
</config>
```

Don't forget chown!

## Tools

### do_nothing.sh

Does nothing, but for 3CX a script is needed for the IVR in order to play a voice message only

### fritzbox_blocklist_lesen.sh

Reads the blocklist from FB and saves the XML data to file.
That file is used by several scripts.

### fritzbox_telefonbuch_lesen.sh

Reads the phone book of the user / phone book ID is defined in the script and must be adapted accordingly.

### first_call_check_privat.sh

The script is called by the IVR of the 3CX with no user input.
It will return a number representing the category assigned to the call.

Categories (ID):
* 11: Known caller (exists in phone book)
* 12: Non anonymous caller that is not found in phone book
* 13: Anonymous caller
* 14: Caller that was found on the block list

### unbekannte_nummer_check.sh

Script processes the number given to it by argument via the IVR DTMF input.

* Linked mobile phones will receive a push message via the `send_info_to_phone.sh`

### send_info_to_phone.sh

Send notification to phones linked to Piag group.

* The given number will be checked against the phone book
* Message will contain name and number of the caller and the called number if available

### tellows_check.sh

Check a given number for information on the tellows portal
Information will be pushed to phones via `send_message_to_phone.sh`

* 11: Score neutral - no info
* 12: Score neutral or good (>5) and info
* 13: Score negativ and info
* 14: Invalid request

### send_message_to_phone.sh

Send notification to phones linked to Piag group.
