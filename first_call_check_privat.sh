#!/bin/bash

# Script checks the number for potential dangerous calls and gives back call category:
# categories:
# 11. normal call with known caller
# 12. normal call with not registered caller
# 13. anonomous caller
# 14. blocked caller
# 15. Tellows good
# 16. Tellows bad

resultNormalKnown=11
resultNormalNotRegistered=12
resultAnonomous=13
resultBlocked=14
resultTellowsGood=15
resultTellowsBad=16

logFile="/var/log/3cx_automation.log"
latestCallerID="/var/3cx/latestCallerID.txt"
function log() {
  echo "$(date +%y/%m/%d_%H:%M:%S) [FCC] $*" >>${logFile}
}

log "_____________________________________________"
log "started script"

# Set log file path
blockFile="/var/3cx/FRITZ_blocklist.xml"

CallerID=$1
# CallerID=${CallerID##*+}
# Show script name and variables
log "Ran script private check $0 with parameters:"
log "CallerID: $CallerID, DIDNumber: $2, CallerInput: $3"

if [[ -z "$CallerID" ]]; then
  log "error: number null"
  exit 0
elif [[ -n "$CallerID" ]]; then
  # Save all known caller information to file
  echo "$CallerID ; $2" >|$latestCallerID
  CallerIDde=$( echo "$CallerID" | sed -e "s/\ //g" -e "s/+49/0/" -e "s/+/00/" -e "s/[^0-9*#]//g" )
  log "$CallerIDde"

  if [[ "$CallerID" == "anonymous" ]]; then
    # anonomous number
    log "result: anonomous number"
    exit $resultAnonomous
  elif [[ "$CallerID" != "anonymous" ]]; then
    log "Found number, check block"
    if grep -q "$CallerIDde" "$blockFile"; then
      # caller blocked
      log "result: blocked caller"
      exit $resultBlocked
    fi

    # Lookup name in phonebook to check whether number is known
    name=$(python3 /var/3cx/name_lookup_by_number.py "$CallerID" 2>&1)

    if [[ $name == "unknown" ]]; then
      log "result: not registered number"
      log "get tellows result"
      tellows=$(exec bash /var/3cx/tellows_check.sh "$CallerID")
      tellowsResult=$?
      log "tellowsResult: $tellowsResult"

      if [[ $tellowsResult == 11 ]]; then
        log "result: normal not registered number"
        exit $resultNormalNotRegistered
      elif [[ $tellowsResult == 12 ]]; then
        log "result: tellows good number"
        exit $resultTellowsGood
      elif [[ $tellowsResult == 13 ]]; then
        log "result: tellows bad number"
        exit $resultTellowsBad
      elif [[ $tellowsResult == 14 ]]; then
        log "result: anonymous number?"
        exit $resultNormalNotRegistered
      elif [[ $tellowsResult == 15 ]]; then
        log "result: whitelisted number"
        exit $resultNormalKnown
      fi

    elif [[ $name != "unknown" ]]; then
      log "result: known number"
      exit $resultNormalKnown
    fi
  fi
fi

# Return exit code
exit 0
