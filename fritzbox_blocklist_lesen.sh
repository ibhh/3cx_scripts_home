#!/bin/bash

set -xv
echo Test
# Telefonbuch der Fritzbox in XML-Datei auslesen
PATH=/usr:/usr/bin:/bin
ziel_pfad="/var/3cx"
ziel_datei="FRITZ_blocklist.xml"

config_file_fritzBox="/var/3cx/config_fritzbox.xml"

# FritzBox credentials
_FBOX="$(xmlstarlet sel -t -v '/config/fritzBox/host' "$config_file_fritzBox")"
BoxPW="$(xmlstarlet sel -t -v '/config/fritzBox/password' "$config_file_fritzBox")"
BoxUSER="$(xmlstarlet sel -t -v '/config/fritzBox/user' "$config_file_fritzBox")"
# default 0
# blocklist 1
# import/sync 240
_PhonebookId="$(xmlstarlet sel -t -v '/config/fritzBox/phoneBookBlocklistId' "$config_file_fritzBox")"
_PhonebookExportName="$(xmlstarlet sel -t -v '/config/fritzBox/phoneBookBlocklistExportName' "$config_file_fritzBox")"

echo "setup done"
# get challenge key from FB

SID=""
getSID(){
  location="/upnp/control/deviceconfig"
  uri="urn:dslforum-org:service:DeviceConfig:1"
  action='X_AVM-DE_CreateUrlSID'

  SID=$(curl -s -k -m 5 --anyauth -u "$BoxUSER:$BoxPW" "http://${_FBOX}:49000$location" -H 'Content-Type: text/xml; charset="utf-8"' -H "SoapAction:$uri#$action" -d "<?xml version='1.0' encoding='utf-8'?><s:Envelope s:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/' xmlns:s='http://schemas.xmlsoap.org/soap/envelope/'><s:Body><u:$action xmlns:u='$uri'></u:$action></s:Body></s:Envelope>" | grep "NewX_AVM-DE_UrlSID" | awk -F">" '{print $2}' | awk -F"<" '{print $1}' | awk -F"=" '{print $2}')
}

getSID

echo "SID: ${SID}"

# get configuration from FB and write to STDOUT
curl -s \
  -k \
  --form 'sid='"${SID}" \
  --form 'PhonebookId='"${_PhonebookId}" \
  --form 'PhonebookExportName='"${_PhonebookExportName}" \
  --form 'PhonebookExport=' \
  "http://${_FBOX}/cgi-bin/firmwarecfg" \
  -o ${ziel_pfad}/${ziel_datei}
