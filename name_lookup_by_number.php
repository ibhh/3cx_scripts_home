<?php

/**
 * This script checks all fritzbox exports in the same directory
 * against a given number and returns the number owner if found
 * Otherwise number is returned again.
 */
$absolutePath = realpath(dirname(__FILE__)) . '/';
$xmlPath = $absolutePath;
$exportSchema = 'fritzexport';
$lookup = str_replace(array('+49', ' ', '/'), array('0', '', ''), $_REQUEST['nr']);

$files = array();
if (is_dir($absolutePath)) {
    if ($handle = opendir($absolutePath)) {
        // einlesen der Verzeichnisses
        while (($file = readdir($handle)) !== false) {
            if (substr($file, 0, strlen($exportSchema)) === $exportSchema) {
                $files[] = $file;
            }
        }
        closedir($handle);
    }
}
foreach ($files as $file) {
    $xml = simplexml_load_file($xmlPath . $file);
    if ($xml === FALSE) {
        echo "Fehler beim Laden des XML";
        foreach (libxml_get_errors() as $error) {
            echo "<br>", $error->message;
        }
    } else {

        foreach ($xml->phonebook->contact as $element) {
            $name = $element->person->realName;
            $numbers = array();
            foreach ($element->telephony as $e) {
                foreach ($e->number as $number) {
                    // Remove +49 and replace with 0
                    $number = str_replace(array('+49', ' ', '/'), array('0', '', ''), $number);
                    if ($number == $lookup) {
                        echo $name;
                        exit;
                    }
                }
            }
        }
    }
}
echo $lookup;
?>