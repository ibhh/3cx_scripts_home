"""
This script checks all FritzBox exports in the same directory
against a given number and returns the number owner if found
Otherwise number is returned again.
"""
import sys
import xml.dom.minidom


def get_text(nodelist):
    rc = []
    for node in nodelist.childNodes:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.nodeValue)
    return ''.join(rc)


def main():
    if len(sys.argv) == 0:
        exit()

    number_lookup = sys.argv[1]
    for r in (("+49", "0"), (" ", ""), ("/", "")):
        number_lookup = number_lookup.replace(*r)

    doc = xml.dom.minidom.parse("/var/3cx/FRITZ_telefonbuch.fritzexport")

    books = doc.getElementsByTagName("phonebooks")
    for book in books:
        contacts = book.getElementsByTagName("contact")
        for person in contacts:
            telephony = person.getElementsByTagName("telephony")
            for tel in telephony:
                numbers = tel.getElementsByTagName("number")
                for number in numbers:
                    number = get_text(number)
                    for r in (("+49", "0"), (" ", ""), ("/", "")):
                        number = number.replace(*r)

                    if number_lookup == number:
                        name = person.getElementsByTagName("realName")[0]
                        sys.exit(get_text(name))

    sys.exit("unknown")


if __name__ == '__main__':
    main()
