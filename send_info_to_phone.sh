#!/bin/bash

#set -xv
PATH=/usr:/usr/bin:/bin
logFile="/var/log/3cx_automation.log"
function log() {
	echo "$(date +%y/%m/%d_%H:%M:%S) [SITP] $*" >> ${logFile}
}

config_file_piag="/var/3cx/config_piag.xml"

# Piag credentials
piagHost="$( xmlstarlet sel -t -v '/config/piag/host' "$config_file_piag" )"
groupIds="$1"
userName="$( xmlstarlet sel -t -v '/config/piag/username' "$config_file_piag" )"
password="$( xmlstarlet sel -t -v '/config/piag/password' "$config_file_piag" )"

name=$(python3 /var/3cx/name_lookup_by_number.py "$2" 2>&1)

message="Anruf von $name ($2) auf $3"

log "sending notification: $message"
result=$( curl -s -S -v -X POST \
  --connect-timeout 3 \
	--data-urlencode "username=$userName" \
	--data-urlencode "password=$password" \
	--data-urlencode "from=3cx" \
	--data-urlencode "groups=$groupIds" \
	--data-urlencode "message=$message" \
	"https://$piagHost/api/v1/notification/send.php" 2>&1 )


#result=`wget -O - --post-data="$POSTDATA" "https://${_Piag}/api/v1/notification/send.php" 2>/dev/null`

log "$result"

