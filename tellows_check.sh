#!/bin/bash

# Script checks the tellows info
# categories:
# 11. Score neutral - no info
# 12. Score neutral or good (>5) and info
# 13. Score negativ and info
# 14. Invalid request
resultNeutralNoInfo=11
resultNeutralAndInfo=12
resultNegativAndInfo=13
resultInvalidRequest=14
resultWhiteList=15

logFile="/var/log/3cx_automation.log"
latestCallerID="/var/3cx/latestCallerID.txt"
function log() {
  echo "$(date +%y/%m/%d_%H:%M:%S) [TELLOWS] $*" >>${logFile}
}

whiteListFile="/var/3cx/whiteListFile.txt"


CallerID=$1
# CallerID=${CallerID##*+}
log "Ran script tellows check $0 with parameters:"
log "CallerID: $CallerID"

CallerID=$(echo "$1" | sed "s/\ //g")

if [[ -z "$CallerID" ]]; then
  log "error: number null"
  exit 0
elif [[ -n "$CallerID" ]]; then
  # Save all known caller information to file
  echo "$CallerID ; $2" >|$latestCallerID

  if [[ "$CallerID" == "anonymous" ]]; then
    # anonomous number
    log "result: anonomous number"
    exit $resultInvalidRequest
  elif [[ "$CallerID" != "anonymous" ]]; then
    CallerID=$( echo "$CallerID" | sed -e "s/\ //g" -e "s/+49/0/" -e "s/+/00/" -e "s/[^0-9*#]//g" )

    log "Found number $CallerID, check tellows"

    if grep -q "$1" "$whiteListFile"; then
      # caller blocked
      log "result: whitelisted caller"
      exit $resultWhiteList
    fi

    tellowsResult=$(lynx -connect_timeout=5 -dump "http://www.tellows.de/basic/num/$CallerID")
    log "tellowsResult: $tellowsResult"
    tellowsScore=$(echo "$tellowsResult" | sed -ne '/Score:/{s/.*.: //p}')
    log "score: $tellowsScore"
    tellowsComments=$(echo "$tellowsResult" | sed -ne '/Anzahl Bewertungen:/{s/.*.: //p}')
    log "comments: $tellowsComments"
    tellowsCount=$(echo "$tellowsResult" | sed -ne '/Suchanfragen:/{s/.*.: //p}')
    log "count: $tellowsCount"
    tellowsFirma=$(echo "$tellowsResult" | sed -ne '/Name \/ Firma:/{s/.*.: //p}')
    log "company: $tellowsFirma"

    if [[ $tellowsScore =~ ^[1-9]$ ]]; then
      log "Valid result returned."
    else
      log "Invalid result returned (\"$tellowsScore\"), setting SPAMSCORE to neutral."
      tellowsScore=5
    fi

    message=$(exec bash /var/3cx/send_message_to_phone.sh "5" "S: $tellowsScore Nummer: $CallerID F: $tellowsFirma CM: $tellowsComments C: $tellowsCount")

    if [[ $tellowsCount == 0 ]]; then
      log "neutral score no info"
      exit $resultNeutralNoInfo
    elif [[ $tellowsCount -ge 0 ]]; then
      if [[ $tellowsScore -le 5 ]]; then
        log "neutral score and info"
        exit $resultNeutralAndInfo
      elif [[ $tellowsScore -gt 5 ]]; then
        log "negativ score"
        exit $resultNegativAndInfo
      fi

      exit $resultInvalidRequest
    fi
  fi
fi

log _____________________________________________

# Return exit code
exit 0
