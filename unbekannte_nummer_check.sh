#!/bin/bash

# Script proccesses the given number

resultNormalNotRegistered=12
resultBlocked=14
resultTellowsGood=15
resultTellowsBad=16

logFile="/var/log/3cx_automation.log"
function log() {
	echo "$(date +%y/%m/%d_%H:%M:%S) [UBK] $*" >> ${logFile}
}

log "started script"
latestCallerID="/var/3cx/latestCallerID.txt"
blockFile="/var/3cx/FRITZ_blocklist.xml"

latestCaller=$(cat $latestCallerID)
IFS=' ; ' read -r -a dataValues <<< "$latestCaller"
callLine=${dataValues[1]}

CallerID=$3

# Show script name and variables
log "Ran script $0 with number: $CallerID"


if [[ -z "$CallerID" ]]; then
	log "empty number"
	exit 1
elif [[ -n "$CallerID" ]]; then
	echo "$CallerID ; $callLine" >| $latestCallerID
	log "Found number, sending notification"
	result=$(exec bash /var/3cx/send_info_to_phone.sh "5" "$CallerID" "$callLine")

	log "Found number, check block"
  if grep -q "$1" "$blockFile"; then
    # caller blocked
    log "result: blocked caller"
    exit $resultBlocked
  fi

	log "get tellows result"
  tellows=$(exec bash /var/3cx/tellows_check.sh "$CallerID")
  tellowsResult=$?
  log "tellows: $tellows"
  log "tellowsResult: $tellowsResult"

  if [[ $tellowsResult == 11 ]]; then
    exit $resultNormalNotRegistered
  elif [[ $tellowsResult == 12 ]]; then
    exit $resultTellowsGood
  elif [[ $tellowsResult == 13 ]]; then
    exit $resultTellowsBad
  elif [[ $tellowsResult == 14 ]]; then
    exit $resultNormalNotRegistered
  elif [[ $tellowsResult == 15 ]]; then
    log "result: whitelisted number"
    exit $resultNormalNotRegistered
  fi

	exit 0
fi

log "_____________________________________________"
# Return exit code
exit 1
